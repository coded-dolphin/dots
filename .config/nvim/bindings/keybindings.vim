nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

nnoremap <space>/ :Commentary<CR>
vnoremap <space>/ :Commentary<CR>

map <C-n> :NERDTreeToggle<CR>
map <C-t> :TransparentToggle<CR>
