source $HOME/.config/nvim/general/settings.vim
source $HOME/.config/nvim/plugins/plugins.vim
source $HOME/.config/nvim/bindings/keybindings.vim
source $HOME/.config/nvim/themes/gruv.vim
source $HOME/.config/nvim/nerd-tree/nerd-tree.vim
source $HOME/.config/nvim/themes/airline.vim
source $HOME/.config/nvim/plug-config/start-screen.vim
luafile $HOME/.config/nvim/lua/plug-colorizer.lua
