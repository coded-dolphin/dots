let g:mapleader = "\<Space>"

filetype plugin indent on
syntax enable
set number 

set wrap linebreak nolist

set t_Co=256

set cursorline
"hi CursorLine cterm=NONE ctermbg=grey ctermfg=white guibg=grey guifg=white

set mouse=nicr

set splitbelow splitright
set termguicolors

set laststatus=2
