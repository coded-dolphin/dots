let g:mapleader = "\<Space>"


filetype plugin indent on
syntax enable
set number

set wrap linebreak nolist

set cursorline
hi CursorLine cterm=NONE ctermbg=grey ctermfg=white guibg=grey guifg=white

set t_Co=256
set termguicolors

set mouse=nicr

set splitbelow splitright

set expandtab
set smarttab

set laststatus=2
